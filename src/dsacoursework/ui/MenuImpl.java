/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.ui;

import dsacoursework.Util;
import dsacoursework.part2.Album;
import dsacoursework.part2.AlbumComparator;
import dsacoursework.part2.Sort;
import dsacoursework.part3.AlbumStack;
import dsacoursework.part4.AlbumData;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 *
 * @author Dom
 */
public class MenuImpl {

    /**
     *
     */
    public static void init() {
        MenuImpl.MainMenu();
    }

    /**
     *
     */
    public static void MainMenu() {
        MenuManager menu = new MenuManager("Main Menu", new LinkedHashMap<String, AnonParam>() {
            {
                put("Playlists", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.Playlists();
                        MenuImpl.MainMenu();
                    }
                });
                
                put("Music Collection", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.MusicCollection();
                        MenuImpl.MainMenu();
                    }
                });
                
                put("Exit", new AnonParam() {
                    @Override
                    public void method() {
                        Util.printOut("Bye! :o)");
                        System.exit(0);
                    }
                });
            }
        });
        
        menu.render();
    }
    
    private static Map<String,AlbumStack> playlists = new HashMap<String, AlbumStack>();
    private static void Playlists() {
        MenuManager menu = new MenuManager("Playlists", new LinkedHashMap<String, AnonParam>() {
            {
                put("New Playlist", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.NewPlaylist();
                        MenuImpl.Playlists();
                    }
                });
                put("Import Playlist", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.ImportPlaylist();
                        MenuImpl.Playlists();
                    }
                });
                put("Select Playlist", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SelectPlaylist();
                        MenuImpl.Playlists();
                    }
                });
                put("Main Menu", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.MainMenu();
                    }
                });
                
            }
        });
        
        menu.render();
    }
    
    private static void NewPlaylist() {
        String name = Util.readString("Enter playlist name: ");
        while( playlists.containsKey(name) ){
            Util.printErr("Sorry! That playlist name has been taken!");
            name = Util.readString("Enter playlist name: ");
        }
        
        AlbumStack playlist = new AlbumStack();
        boolean populate = Util.readYesNo("Do you want to pre-populate the playlist with random tracks?");
        if( populate ){
            playlist.populate( AlbumData.loadData() );
        }
        
        playlists.put(name, playlist);
        Util.printOut("Playlist created sucessfully!");
    }
    
    private static void ImportPlaylist() {
        String name = Util.readString("Enter file name: ");
        File file = new File( name );
        
        if( file.exists() ){
            if( file.canRead() ){
                AlbumStack playlist = new AlbumStack();
                
                Util.printOut( "Please note: only the first " + playlist.getMaxSize() + " items will be placed into the playlist." );
                List<Album> albums = AlbumData.loadData(name);
                
                for(Album a : albums){
                    playlist.push(a);
                }
                
                playlists.put( file.getName() , playlist );
                 Util.printOut("Playlist " + file.getName() + " imported sucessfully!");
            }else{
                Util.printErr("Sorry that file isnt readable... please check permissions!");
            }
        }else{
            Util.printErr("Sorry that file doesn't exist!");
        }
    }
    
    private static void SelectPlaylist() {
        if(playlists.size() < 1){
             Util.printErr("Sorry! You dont seem to have created or loaded any playlists!");
             return;
        }
        
        MenuManager menu = new MenuManager("Playlists", new LinkedHashMap<String, AnonParam>() {
            {
                for ( Map.Entry<String,AlbumStack> a : playlists.entrySet() ) {
                    put("Playlist - " + a.getKey(), new AnonParam() {
                        @Override
                        public void method() {
                            MenuImpl.ManagePlaylist( a.getValue() );
                        }
                    });
                }
                put("Back", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.Playlists();
                    }
                });
            }
        });
        menu.render();
    }
    private static void ManagePlaylist( AlbumStack a ) {
        
        MenuManager menu = new MenuManager("Manage Playlist", new LinkedHashMap<String, AnonParam>() {
            {
                put("Add Random Album", new AnonParam() {
                    @Override
                    public void method() {
                        Album al = AlbumData.randomTrack();
                        a.push( al );
                        
                        Util.printOut("Added " + al.getAlbumTitle() + " to playlist!");
                        MenuImpl.ManagePlaylist(a);
                    }
                });
                
                put("Pop Topmost Album w/ Details", new AnonParam() {
                    @Override
                    public void method() {
                        Album al = (Album) a.pop();
                        
                        Util.printOut("Popped " + al.getAlbumTitle() + " from playlist!");
                        Util.printOut( al );
                        
                        MenuImpl.ManagePlaylist(a);
                    }
                });
                
                put("View Random Album Details", new AnonParam() {
                    @Override
                    public void method() {
                        Album al = (Album) a.random();
                        
                        Util.printOut("Got " + al.getAlbumTitle() + " from playlist!");
                        Util.printOut( al );
                        
                        MenuImpl.ManagePlaylist(a);
                    }
                });
                
                put("Export Playlist to File", new AnonParam() {
                    @Override
                    public void method() {
                        Util.printDebug( a.export() );
                        
                        String name = Util.readString("Enter file name: ");
                        File file = new File( name );
                        
                        try{
                            if( !file.exists() ){
                                file.createNewFile();
                            }

                            if( file.canWrite() ){
                                boolean overwrite = Util.readYesNo("File will be overwritten if it exists.. continue?");
                                if( overwrite ){
                                    try(  PrintWriter out = new PrintWriter( file )  ){
                                        out.println( a.export() );
                                    } catch ( FileNotFoundException ex ) {
                                        Util.printErr( "Sorry! " + ex.getMessage() );
                                        Util.printDebug( ex );
                                    }
                                }
                            }else{
                                Util.printErr( "Sorry! File you have specified is not writeable.. please check permissions." );
                                Util.printDebug( "Unable to write to file :: " + name );
                            }
                        }catch( IOException ex ){
                            Util.printErr( "Sorry! File you have specified is not writeable.. please check permissions." );
                            Util.printDebug( ex );
                        }
                        
                        MenuImpl.ManagePlaylist(a);
                    }
                });
                
                put("Back", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.Playlists();
                    }
                });
            }
        });
        menu.render();
    }
    
    private static void MusicCollection() {
        MenuManager menu = new MenuManager("Music Collection", new LinkedHashMap<String, AnonParam>() {
            {
                put("Search by Album Artist Name", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SearchMusicCollectionByArtist();
                        MenuImpl.MusicCollection();
                    }
                });
                put("Search by Album Year", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SearchMusicCollectionByYear();
                        MenuImpl.MusicCollection();
                    }
                });
                put("Sort by Album Position", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SearchMusicCollectionByYear();
                        MenuImpl.MusicCollection();
                    }
                });
                put("Sort by Title (ASC)", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SortMusicCollectionByRecordTitle( Sort.ASCENDING );
                        MenuImpl.MusicCollection();
                    }
                });
                put("Sort by Title (DESC)", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SortMusicCollectionByRecordTitle( Sort.DESCENDING );
                        MenuImpl.MusicCollection();
                    }
                });
                put("Sort by Record Label (ASC)", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SortMusicCollectionByRecordTitle( Sort.ASCENDING );
                        MenuImpl.MusicCollection();
                    }
                });
                put("Sort by Record Label (DESC)", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.SortMusicCollectionByRecordTitle( Sort.DESCENDING );
                        MenuImpl.MusicCollection();
                    }
                });
                put("Main Menu", new AnonParam() {
                    @Override
                    public void method() {
                        MenuImpl.MainMenu();
                    }
                });
            }
        });
        
        menu.render();
    }
    
    private static void SearchMusicCollectionByArtist(){
        String name = Util.readString("Enter artist name: ").toLowerCase();
        
        List<Album> collection = AlbumData.loadData();
        List<Album> res = collection.stream().filter( a -> a.getAlbumArtist().toLowerCase().contains(name)).collect(Collectors.toList());
        
        if(res.size() > 0){
            Util.printOut("Search results for :: " + name + System.lineSeparator());
            for(Album a : res){
                Util.printOut(a.toString().replaceAll("(?i)"+name,"\u001B[31m"+name+"\u001B[0m"));
            }
        }else{
            Util.printOut("No results found for :: " + name + System.lineSeparator());
        }
    }
    
    private static void SearchMusicCollectionByYear(){
        int year = Util.readInt("Enter album year: ", 2016, 1800);
        
        List<Album> collection = AlbumData.loadData();
        List<Album> res = collection.stream().filter( a -> a.getAlbumYear() == year ).collect(Collectors.toList());
        
        if(res.size() > 0){
            Util.printOut("Search results for year :: " + year + System.lineSeparator());
            for(Album a : res){
                Util.printOut(a.toString().replaceAll("(?i)"+year,"\u001B[31m"+year+"\u001B[0m"));
            }
        }else{
            Util.printOut("No results found for year :: " + year + System.lineSeparator());
        }
    }
    
    private static void SortMusicCollectionByRecordTitle( Sort sort ){
        List<Album> res = AlbumData.loadData();
        Collections.sort( res, AlbumComparator.sortByTitle( sort ) );
        
        Util.printOut("Albums in collection sorted by Record Title in " + sort.namedValue() + " order." + System.lineSeparator());
        for( Album a : res ){
            Util.printOut(a.toString());
        }
    }
    private static void SortMusicCollectionByRecordLabelName( Sort sort ){
        List<Album> res = AlbumData.loadData();
        Collections.sort( res, AlbumComparator.sortByRecordLabel( sort ) );
        
        Util.printOut("Albums in collection sorted by Record Label Name in " + sort.namedValue() + " order." + System.lineSeparator());
        for( Album a : res ){
            Util.printOut(a.toString());
        }
    }
}