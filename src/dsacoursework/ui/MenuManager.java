/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.ui;

import dsacoursework.Util;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Dom
 */
public class MenuManager {
   private final String title;
   private final LinkedHashMap<String, AnonParam> menuItems;
   
    /**
     *
     * @param title
     * @param menuItems
     */
    public MenuManager( String title, LinkedHashMap<String, AnonParam> menuItems ){
       this.title = title;
       this.menuItems = menuItems;
   }
   
   private int maxLength(){
       
       return 50;
       /*
       OptionalInt in = this.menuItems.keySet().stream().mapToInt( i -> i.length() ).max();
       if(in.isPresent()){
           return in.getAsInt();
       }
       
       return 0;*/
   }
   
    /**
     *
     * @param title
     * @param max
     */
    public static void printTitle(String title, int max){
        
       Util.printOut( "" );
       Util.printOut( String.format("%-"+max+"s", title.replace(" ", "|")+"|").replace(' ', '=').replace("|", " ").trim() );
   }
   
    /**
     *
     */
    public void render(){
        int m = this.maxLength();
        m = m+3+(m/10);
 
        //Print Title
        this.printTitle(title, m);
       
        int i=1;
        for( Map.Entry<String, AnonParam> item : this.menuItems.entrySet()){
           Util.printOut( String.format("%-"+m+"s", i + ". " + item.getKey().trim() ) );
           i++;
        }
       
        int option = Util.readInt(
            "Please choose an option : ",
            this.menuItems.size(),
            1 
        );
        Util.printOut( "" );
        
        AnonParam item = this.menuItems.get(this.menuItems.keySet().toArray()[option-1] );
        
        Util.printOut( String.format("%-"+m+"s", "").replace(' ', '=').trim() );
        Util.printOut( "" );
        item.method();
        Util.printOut( String.format("%-"+m+"s", "").replace(' ', '=').trim() );
        
   }
}
