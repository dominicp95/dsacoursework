/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework;

import dsacoursework.tests.part1.SelectorTest;
import dsacoursework.tests.part3.AlbumStackTest;
import dsacoursework.tests.part4.AlbumDataTest;

/**
 *
 * @author Dom
 */
public class DSACourseworkTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SelectorTest selectorTest = new SelectorTest();
        selectorTest.run();
        
        AlbumDataTest albumDataTest = new AlbumDataTest();
        albumDataTest.run();
        
        AlbumStackTest albumStackTest = new AlbumStackTest();
        albumStackTest.run();
    }
    
}
