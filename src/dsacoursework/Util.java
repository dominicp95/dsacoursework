/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework;

import java.util.Scanner;

/**
 *
 * @author Dominic Porteous
 */
public class Util {
    
    public static boolean DEBUG = false;
    
    /**
     *
     * @param o
     */
    public static void printOut(Object o){
        System.out.println(o);
    }

    /**
     *
     */
    public static void printReturn(){
        System.out.println("");
    }
    
    /**
     *
     * @param o
     */
    public static void printDebug(Object o){
        if( DEBUG ){ System.out.println("\u001B[34m" + o + "\u001B[0m"); }
    }

    /**
     *
     */
    public static void printDebugLine(){
        if( DEBUG ){ System.out.println(""); }
    }
    
    /**
     *
     * @param o
     */
    public static void printErr(Object o){
        System.err.println("\u001B[31m" + o + "\u001B[0m");
        System.err.println(" ");
    }
    
    // Read String

    /**
     *
     * @param prompt
     * @return
     */
    public static String readString(String prompt) {
        Scanner input = new Scanner(System.in);
        
        System.out.print(prompt);
        String inputText = input.nextLine();
        return inputText;
    }
    
    /**
     *
     * @param prompt
     * @return
     */
    public static boolean readYesNo(String prompt) {
        String s = readString( prompt + " [y/n] ");
        while( !s.toLowerCase().equals( "y" ) && !s.toLowerCase().equals( "n" )){
            s = readString( prompt );
        }
        
        return s.toLowerCase().equals( "y" );
    }
    
    // Read Int

    /**
     *
     * @param prompt
     * @return
     */
        public static int readInt(String prompt) { return readInt(prompt, Integer.MAX_VALUE, 1); }

    /**
     *
     * @param prompt
     * @param max
     * @param min
     * @return
     */
    public static int readInt(String prompt, int max, int min) {
        int inputNumber = 0;
        boolean inputError;
        Scanner input = new Scanner(System.in);
        do {
            inputError = false;                
            System.out.print(prompt);

            try {
                inputNumber = Integer.parseInt(input.nextLine());
                if (inputNumber < min || inputNumber > max) {
                    inputError = true;
                    System.out.println("Number out of range: please re-enter");                        
                }
            } catch (NumberFormatException e) {
                inputError = true;
                System.out.println("Not a valid number: please re-enter");
            }
        } while (inputError);
        return inputNumber;
    } 
    
    // Read Long

    /**
     *
     * @param prompt
     * @return
     */
        public static long readLong(String prompt) { return readLong(prompt, Long.MAX_VALUE, 1); }

    /**
     *
     * @param prompt
     * @param max
     * @param min
     * @return
     */
    public static long readLong(String prompt, long max, long min) {
        long inputNumber = 0;
        boolean inputError;
        Scanner input = new Scanner(System.in);
        do {
            inputError = false;                
            System.out.print(prompt);

            try {
                inputNumber = Long.parseLong(input.nextLine());
                if (inputNumber < min || inputNumber > max) {
                    inputError = true;
                    System.out.println("Number out of range: please re-enter");                        
                }
            } catch (NumberFormatException e) {
                inputError = true;
                System.out.println("Not a valid number: please re-enter");
            }
        } while (inputError);
        return inputNumber;
    } 
}

