/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.part4;

import dsacoursework.part1.Selector;
import dsacoursework.part2.Album;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Dom
 */
public class AlbumData {
    public static final String filename = "Top500Albums.txt";
    
    public static List<Album> loadData(){
        return loadData( filename );
    }   
    
    public static List<Album> loadData( String filename ){
        List<Album> data = new ArrayList<Album>();
        File file = new File( filename );

        try( Scanner s = new Scanner( file ) ){
            while (s.hasNextLine()) {
                if(!s.nextLine().trim().equals("")){
                    try{
                        Album a = new Album();
                        a.setAlbumArtist( s.nextLine() );
                        a.setAlbumTitle( s.nextLine() );
                        a.setAlbumRecordLabel( s.nextLine() );
                        a.setAlbumYear( Integer.parseInt(s.nextLine()) );
                        
                        data.add( a );
                    } catch ( Exception e ) {
                        e.printStackTrace();
                    }
                }
            }
        } catch ( FileNotFoundException e ) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public static Album randomTrack(){
        List<Album> data = loadData();
        
        Selector s = new Selector();
        s.selectorRange( 0, data.size() );

        return data.get(s.selectorValue());
    }
}
