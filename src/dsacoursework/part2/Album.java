/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.part2;

/**
 *
 * @author Dom
 */
public class Album {
    
    public Album(){
        
    }
    
    private int albumYear;
    public int getAlbumYear( ) {
        return albumYear;
    }
    public void setAlbumYear( int albumYear ) {
        this.albumYear = albumYear;
    }
    
    private String albumArtist;
    public String getAlbumArtist( ) {
        return albumArtist;
    }
    public void setAlbumArtist( String albumArtist ) {
        this.albumArtist = albumArtist;
    }

    private String albumTitle;
    public String getAlbumTitle( ) {
        return albumTitle;
    }
    public void setAlbumTitle( String albumTitle ) {
        this.albumTitle = albumTitle;
    }

    private String albumRecordLabel;
    public String getAlbumRecordLabel( ) {
        return albumRecordLabel;
    }
    public void setAlbumRecordLabel( String albumRecordLabel ) {
        this.albumRecordLabel = albumRecordLabel;
    }
    
    @Override
    public String toString(){
        return toString(false);
    }
    
    public String toString( boolean raw ){
        if( raw ){
            return this.getAlbumTitle() + System.lineSeparator()
                 + this.getAlbumArtist() + System.lineSeparator()
                 + this.getAlbumRecordLabel()+ System.lineSeparator()
                 + this.getAlbumYear() + System.lineSeparator();
        }else{
            return "Album Title: " + this.getAlbumTitle() + System.lineSeparator()
                 + "Album Artist: " + this.getAlbumArtist() + System.lineSeparator()
                 + "Record Label: " + this.getAlbumRecordLabel()+ System.lineSeparator()
                 + "Year of Release: " + this.getAlbumYear() + System.lineSeparator();
        }
    }
}
