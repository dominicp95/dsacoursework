/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.part2;

/**
 *
 * @author Dom
 */
public enum Sort {

    /**
     * Sort direction: Ascending
     * 
     */
    ASCENDING(1),

    /**
     * Sort direction: Descending
     * 
     */
    DESCENDING(2);
    
    int val;
    
    Sort(int val){
        this.val = val;
    }
    
    @Override
    public String toString(){
        return Integer.toString( this.val );
    }
    
    public String namedValue(){
        return this.name();
    }
}