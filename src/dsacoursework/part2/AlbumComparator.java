/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.part2;

import java.util.Comparator;

/**
 *
 * @author Dom
 */
public class AlbumComparator {
    
    /**
     * Returns comparator used when sorting album objects, using specified Sort direction.
     * 
     * @param sort
     * @return Comparator<Album>
     */
    public static Comparator<Album> sortByTitle(Sort sort){
        return new Comparator<Album>(){
            @Override
            public int compare(Album o1, Album o2) {
                int val = (o1.getAlbumTitle().compareTo(o2.getAlbumTitle()));
                
                if(sort == Sort.ASCENDING){
                    return val;
                }else{
                    return val*(-1);
                }
            }
        };
    }
    
    /**
     * Returns comparator used when sorting album objects, using specified Sort direction.
     * 
     * @param sort
     * @return Comparator<Album>
     */
    public static Comparator<Album> sortByRecordLabel(Sort sort){
        return new Comparator<Album>(){
            @Override
            public int compare(Album o1, Album o2) {
                int val = (o1.getAlbumRecordLabel().compareTo(o2.getAlbumRecordLabel()));
                
                if(sort == Sort.ASCENDING){
                    return val;
                }else{
                    return val*(-1);
                }
            }
        };
    }
}
