/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.part1;

/**
 *
 * @author Dom
 */
abstract class ASelector {
    protected int min;
    protected int max;
    
    abstract void selectorRange( int min, int max );
    abstract int selectorValue( );
}
