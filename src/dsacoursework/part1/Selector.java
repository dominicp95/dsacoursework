/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.part1;

import java.util.Random;

/**
 *
 * @author Dom
 */
public class Selector extends ASelector {
    
    @Override
    public void selectorRange( int min, int max ) {
        this.min = min;
        this.max = max;
    }
    
    /*
    *   Note: Min number is inclusive for nextInt so we need to add on 1.
    */
    @Override
    public int selectorValue( ) {
        Random r = new Random();
        return r.nextInt( this.max - ( this.min + 1 ) ) + this.min;
    }
    
}
