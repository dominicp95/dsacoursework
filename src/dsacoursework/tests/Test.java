/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.tests;


/**
 *
 * @author Dom
 */
public abstract class Test {
    public void run(){
        System.out.println( this.getClass().getSimpleName()+" Start ========================================"+ System.lineSeparator());
        this.work();
        System.out.println( this.getClass().getSimpleName()+" End =========================================="+ System.lineSeparator());
    }

    public abstract void work();
    
}
