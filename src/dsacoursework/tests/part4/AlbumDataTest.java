/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.tests.part4;

import dsacoursework.tests.Test;
import dsacoursework.part2.Album;
import dsacoursework.part4.AlbumData;

/**
 *
 * @author Dom
 */
public class AlbumDataTest extends Test{
    @Override
    public void work() {
        System.out.println( "Total num albums loaded :: " + AlbumData.loadData().size() + System.lineSeparator() + System.lineSeparator() );
        System.out.println( "Album Data =============================================="+ System.lineSeparator());
        for( Album a : AlbumData.loadData() ){
            System.out.println( a );
        }
        System.out.println( "Album Data End =========================================="+ System.lineSeparator());
    }
}
