/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.tests.part1;

import dsacoursework.part1.Selector;
import dsacoursework.tests.Test;

import java.util.ArrayList;
import java.util.List;

import java.awt.Point;
/**
 *
 * @author Dom
 */
public class SelectorTest extends Test{
    //Using point class as a placeholder for min/max values to loop through
    private List<Point> cases = new ArrayList<Point>(){{
        add( new Point( 1, 10 ) );
        add( new Point( 10, 100 ) );
        add( new Point( 15, 30 ) );
        add( new Point( 10, 100 ) );
        add( new Point( 0, 100 ) );
        add( new Point( 50, 55 ) );
    }};
    
    @Override
    public void work() {
        for( Point p : cases ){
            Selector s = new Selector();
            s.selectorRange( p.x, p.y );
            System.out.println( "Random Number between " + p.x + " and " + p.y + " :: " + s.selectorValue() + System.lineSeparator() );
        }
    }
}
