/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.tests.part3;

import dsacoursework.part1.Selector;
import dsacoursework.part2.Album;
import dsacoursework.part3.AlbumStack;
import dsacoursework.part4.AlbumData;
import dsacoursework.tests.Test;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Dom
 */
public class AlbumStackTest extends Test{

    @Override
    public void work() {
        List<Album> data = AlbumData.loadData();
        System.out.println( "Total num albums loaded :: " + data.size() + System.lineSeparator() );
        
        for( int i=0; i<10; i++ ){
            AlbumStack stack = new AlbumStack();
            
            for( int c=0; c<10; c++ ){
                Selector s = new Selector();
                s.selectorRange( 0, data.size() );
                
                Album a = data.get(s.selectorValue());
                while( stack.contains(a) ){
                    a = data.get(s.selectorValue());
                }
                stack.push(a);
            }
            
            System.out.println( "Album Stack " + i + " :: Size = " + stack.size() + " ===================================== " + System.lineSeparator() );
            Iterator<Album> it = stack.iterator();
            while (it.hasNext()){
                System.out.println( it.next() + System.lineSeparator()) ;
            }
            System.out.println( "Album Stack " + i + " End :: Size = " + stack.size() + " ================================= " + System.lineSeparator() );
        }
        
    }
    
}
