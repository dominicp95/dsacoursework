/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dsacoursework.part3;

import dsacoursework.part1.Selector;
import dsacoursework.part2.Album;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author Dom
 */
public class AlbumStack extends Stack<Album> {

    private final int stackSize = 8;
    public int getMaxSize(){
        return stackSize;
    }
    
    @Override
    public Album push( Album a ) {
        super.push(a);
        
        while ( this.size() > this.stackSize ) {
            this.removeElementAt( this.size() - 1 );
        }
        
        return null;
    }
    
    public Album random(){
        Selector s = new Selector();
        s.selectorRange(0, this.size());
        
        return this.get(s.selectorValue());
    }
    
    public void populate( List<Album> data ){ 
        for( int c=0; c<10; c++ ){
            Selector s = new Selector();
            s.selectorRange( 0, data.size() );

            Album a = data.get(s.selectorValue());
            while( this.contains(a) ){
                a = data.get(s.selectorValue());
            }
            this.push(a);
        }
    }
    
    public String export(){
        String ret = "";
        int i = 1;
        
        for(Album a : this) {
            ret +=  i + System.lineSeparator();
            ret +=  a.toString( true );
            ret += System.lineSeparator();
            i++;
        }
        
        System.out.println( ret );
        return ret;
    }
 }